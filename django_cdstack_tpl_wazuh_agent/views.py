import xmltodict
from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)


def handle(zipfile_handler, template_opts, cmdb_host, dynamic=True):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_wazuh_agent/django_cdstack_tpl_wazuh_agent"

    if dynamic:
        ossec_conf = open(
            module_prefix + "/templates/config-fs/static/var/ossec/etc/ossec.conf", "rb"
        ).read()
        ossec_conf_dict = xmltodict.parse(ossec_conf)

        if "monitoring_wazuh_master_server" in template_opts:
            ossec_conf_dict["ossec_config"]["client"]["server"]["address"] = (
                template_opts["monitoring_wazuh_master_server"]
            )

        zip_add_file(
            zipfile_handler,
            "var/ossec/etc/ossec.conf",
            xmltodict.unparse(ossec_conf_dict, pretty=True, full_document=False),
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
